function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src=${pictureUrl} class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Starts: ${start}</li>
            <li class="list-group-item">Ends: ${end}</li>
          </ul>
        </div>
      </div>
    </div>
    `;
}

function alertComponent() {
    return `
    <div id="something-wrong" class="alert alert-primary" role="alert">
    Something bad happened
    </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // will go to catch block
        } else {
            const data = await response.json();
            let i = 1;

            for (let conference of data.conferences) {
                if (i > 3) {
                    i = 1;
                }
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    const startISO = details.conference.starts;
                    const startYear = startISO.slice(0, 4);
                    const startMonth = startISO.slice(5, 7);
                    const startDay = startISO.slice(8, 10);
                    const start = `${startMonth}/${startDay}/${startYear}`;

                    const endISO = details.conference.ends;
                    const endYear = endISO.slice(0, 4);
                    const endMonth = endISO.slice(5, 7);
                    const endDay = endISO.slice(8, 10);
                    const end = `${endMonth}/${endDay}/${endYear}`;

                    const location = details.conference.location.name;

                    const html = createCard(name, description, pictureUrl, start, end, location);
                    const column = document.querySelector(`#col${i}`);
                    column.innerHTML += html;
                    i++;
                }
            }

        }
    } catch (e) {
        console.error("ERROR HAPPEN");

        const newHTML = alertComponent();
        const somethingWrong = document.querySelector("#something-wrong");
        somethingWrong.innerHTML = newHTML;
    }

});
